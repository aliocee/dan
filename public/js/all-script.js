/**
 * This is an example on how to use sigma filters plugin on a real-world graph.
*/
var atc = {
        "A":"Alimentary tract and metabolism",
        "B":"Blood and blood forming organs",
        "C":"Cardiovascular system",
        "D":"Dermatologicals",
        "G":"Genito-urinary and sex hormones",
        "H":"Systemic hormonal preparations",
        "J":"Antiinfectives for systemic use",
        "L":"Antineoplastic and immuno. agents",
        "M":"Musculo-skeletal system",
        "N":"Nervous system",
        "P":"Antiparasitic, insecticides & repellents",
        "R":"Respiratory system",
        "S":"Sensory organs",
        "V":"Various",
        "X":"Others",
        "NA":"Not available"
  }

var filter;

/**
 * DOM utility functions
*/
var _ = {
  $: function (id) {
    return document.getElementById(id);
  },

  all: function (selectors) {
    return document.querySelectorAll(selectors);
  },

  removeClass: function(selectors, cssClass) {
    var nodes = document.querySelectorAll(selectors);
    var l = nodes.length;
    for ( i = 0 ; i < l; i++ ) {
      var el = nodes[i];
      // Bootstrap compatibility
      el.className = el.className.replace(cssClass, '');
    }
  },

  addClass: function (selectors, cssClass) {
    var nodes = document.querySelectorAll(selectors);
    var l = nodes.length;
    for ( i = 0 ; i < l; i++ ) {
      var el = nodes[i];
      // Bootstrap compatibility
      if (-1 == el.className.indexOf(cssClass)) {
        el.className += ' ' + cssClass;
      }
    }
  },

  show: function (selectors) {
    this.removeClass(selectors, 'hidden');
  },

  hide: function (selectors) {
    this.addClass(selectors, 'hidden');
  },

  toggle: function (selectors, cssClass) {
    var cssClass = cssClass || "hidden";
    var nodes = document.querySelectorAll(selectors);
    var l = nodes.length;
    for ( i = 0 ; i < l; i++ ) {
      var el = nodes[i];
      //el.style.display = (el.style.display != 'none' ? 'none' : '' );
      // Bootstrap compatibility
      if (-1 !== el.className.indexOf(cssClass)) {
        el.className = el.className.replace(cssClass, '');
      } else {
        el.className += ' ' + cssClass;
      }
    }
  }
};

function updatePane (graph, filter) {
  // get max degree
  var maxDegree = 0,
      categories = {};

  // read nodes

  graph.nodes().forEach(function(n) {
    maxDegree = Math.max(maxDegree, graph.degree(n.id));
    categories[n.attributes.atc_codes_letter] = true;
  })

  // min degree
  _.$('min-degree').max = maxDegree;
  _.$('max-degree-value').textContent = maxDegree;

  // node category
  // categories = Object.keys(categories).sort().reduce((r, k) => (r[k] = categories[k], r), {})
  var nodecategoryElt = _.$('node-category');
  Object.keys(categories).forEach(function(c) {
    var optionElt = document.createElement("option");
    optionElt.text = atc[c];
    optionElt.value = c;
    nodecategoryElt.add(optionElt);
  });

  // reset button
  _.$('reset-btn').addEventListener("click", function(e) {
    _.$('min-degree').value = 0;
    _.$('min-degree-val').textContent = '0';
    _.$('node-category').selectedIndex = 0;
    filter.undo().apply();
    _.$('dump').textContent = '';
    _.hide('#dump');
  });

  // // export button
  // _.$('export-btn').addEventListener("click", function(e) {
  //   var chain = filter.export();
  //   console.log(chain);
  //   _.$('dump').textContent = JSON.stringify(chain);
  //   _.show('#dump');
  // });
}

// Add a method to the graph model that returns an
// object with every neighbors of a node inside:
sigma.classes.graph.addMethod('neighbors', function(nodeId) {
  var k,
      neighbors = {},
      index = this.allNeighborsIndex[nodeId] || {};

  for (k in index)
    neighbors[k] = this.nodesIndex[k];

  return neighbors;
});

var sg, renderer;


function generateLinks(str, type){

  if (str === undefined || str.length == 0 || str === NaN || str === "NA") {
    return '-'
  }

  var res = str.toString().split("|");

  if(type === 'cat') return res[0]
  if(type === 'group') return res[0]
  if(type === 'atc') return res[0]
  if(type === 'type') return res[0]

  if(type === 'lincs'){
    if(res.length === 1){
    return '<a target="_blank" href="http://lincsportal.ccs.miami.edu/SmallMolecules/view/'+res+'">'+res+'</a>'
    }
    else if(res.length === 2){

      return '<a target="_blank" href="http://lincsportal.ccs.miami.edu/SmallMolecules/view/'+res[0]+'">'+res[0]+'</a>, <a target="_blank" href="http://lincsportal.ccs.miami.edu/SmallMolecules/view/'+res[1]+'">'+res[1]+'</a>'
    }
    else{
      return '<a target="_blank" href="http://lincsportal.ccs.miami.edu/SmallMolecules/view/'+res[0]+'">'+res[0]+'</a>, <a target="_blank" href="http://lincsportal.ccs.miami.edu/SmallMolecules/view/'+res[1]+'">'+res[1]+'</a>, <a target="_blank" href="http://lincsportal.ccs.miami.edu/SmallMolecules/view/'+res[2]+'">'+res[2]+'</a>'
    }
  }
  if(type === 'chembl'){
    if(res.length === 1){
    return '<a target="_blank" href="https://www.ebi.ac.uk/chembldb/index.php/compound/inspect/'+res+'">'+res+'</a>'
    }
    else{

      return '<a target="_blank" href="https://www.ebi.ac.uk/chembldb/index.php/compound/inspect/'+res[0]+'">'+res[0]+'</a>', '<a target="_blank" href="https://www.ebi.ac.uk/chembldb/index.php/compound/inspect/'+res[1]+'">'+res[1]+'</a>'
    }
  }
  if(type === 'kegg'){
    if(res.length === 1){
    return '<a target="_blank" href="http://www.genome.jp/dbget-bin/www_bget?cpd:'+res+'">'+res+'</a>'
    }
    else if(res.length === 2){

      return '<a target="_blank" href="http://www.genome.jp/dbget-bin/www_bget?cpd:'+res[0]+'">'+res[0]+'</a>, <a target="_blank" href="http://www.genome.jp/dbget-bin/www_bget?cpd:'+res[1]+'">'+res[1]+'</a>'
    }
    else{
      return '<a target="_blank" href="hhttp://www.genome.jp/dbget-bin/www_bget?cpd:'+res[0]+'">'+res[0]+'</a>, <a target="_blank" href="http://www.genome.jp/dbget-bin/www_bget?cpd:'+res[1]+'">'+res[1]+'</a>, <a target="_blank" href="http://www.genome.jp/dbget-bin/www_bget?cpd:'+res[2]+'">'+res[2]+'</a>'
    }
  }
  if(type === 'pubchem'){
    return '<a target="_blank" href="https://pubchem.ncbi.nlm.nih.gov/compound/'+res+'">'+res+'</a>'
  }
  if(type === 'drugbank'){
    return '<a target="_blank" href="https://www.drugbank.ca/drugs/'+res+'">'+res+'</a>'
  }
}


function activateNode(id) {
  renderer.dispatchEvent('outNode', { node:sg.graph.nodes(id)})
  renderer.dispatchEvent('clickNode', { node:sg.graph.nodes(id)})
}

function hoverNode(id) {
  renderer.dispatchEvent('overNode', { node:sg.graph.nodes(id)})
}

function deactivateNode(id) {
  renderer.dispatchEvent('outNode', { node:sg.graph.nodes(id)})
}

sigma.parsers.gexf(
  fname,
  {
    container: 'graph-container',
    // canvas renderer
    // ===============
    renderer: {
      container: document.getElementById('graph-container'),
      type: sigma.renderers.canvas
    },
    settings: {
      defaultEdgeType: 'curve'
    },
  },
  function(s) {

    // Mine
    $(function() {
        var id = window.location.hash.substr(1);
        if (id.length > 0){
          s.renderers[0].dispatchEvent('clickNode', { node:s.graph.nodes(id)})
        }

        $( '#search' ).on( 'keydown', function ( e ) {
            $( '#attributepane' ).hide()
            $( '.invalid-feedback' ).hide('slow')

            if( e.keyCode == 13 ){
              var keyword = $( this ).val(), flag = 0

              s.graph.nodes().forEach(function(n) {
                  if(n.id === keyword.trim()){
                    flag = 1
                  }
              })
              if(keyword.length > 1 && flag == 1)
                if(flag == 1)
                {
                  s.renderers[0].dispatchEvent('clickNode', { node:s.graph.nodes(keyword)})
                }
                else
                {
                  $( '.invalid-feedback' ).show('slow')
                }
            }
        } );
    });

    renderer = s.renderers[0];
    sg = s;

    s.cameras[0].goTo({ x: 0, y: 0, angle: 0, ratio: 0.5 });

    // Nodes and Edges
    $('.node-count').html(s.graph.nodes().length)
    $('.edge-count').html(s.graph.edges().length)

    // We first need to save the original colors of our
    // nodes and edges, like this:
    s.graph.nodes().forEach(function(n) {
      n.originalColor = n.color;
    });
    s.graph.edges().forEach(function(e) {
      e.originalColor = e.color;
    });


    // When a node is clicked, we check for each node
    // if it is a neighbor of the clicked one. If not,
    // we set its color as grey, and else, it takes its
    // original color.
    // We do the same for the edges, and we only keep
    // edges that have both extremities colored.
    s.bind('clickNode', function(e) {
      // Sidebar
      setTimeout(function(){
      $('#attributepane').show()
      $('.return-button').click(function(){
          $('#attributepane').hide()
          window.location.hash = ''

          s.graph.nodes().forEach(function(n) {
            n.color = n.originalColor
          });

          s.graph.edges().forEach(function(e) {
            e.color = e.originalColor
          });

          // Same as in the previous event:
          s.refresh()
      });

      $('.node-name').html('<a href="#'+e.data.node.id+'" onmouseover="renderer.dispatchEvent(\'overNode\', {node:sg.graph.nodes(\'' + e.data.node.id + '\')})" onmouseout="renderer.dispatchEvent(\'outNode\', {node:sg.graph.nodes(\'' + e.data.node.id + '\')})">'+e.data.node.id+'</a>')

      $('.node-atc').html(atc[e.data.node.attributes.atc_codes_letter])

      $('.node-code').html(generateLinks(e.data.node.attributes.atc_codes, 'atc'))
      $('.node-type').html(generateLinks(e.data.node.attributes.type, 'type'))
      $('.node-group').html(generateLinks(e.data.node.attributes.groups, 'group'))
      $('.node-cat').html(generateLinks(e.data.node.attributes.categories, 'cat'))
      // $('.node-inchikey').html(e.data.node.attributes.inchikey)

      $('.node-db').html(generateLinks(e.data.node.attributes.drugbank_id, 'drugbank'))

      $('.node-lincs').html(generateLinks(e.data.node.attributes.lincs_id, 'lincs'))

      $('.node-pubchem').html(generateLinks(e.data.node.attributes.pubchem_cid, 'pubchem'))

      $('.node-chembl').html(generateLinks(e.data.node.attributes.chembl_id, 'chembl'))

      $('.node-kegg').html(generateLinks(e.data.node.attributes.kegg_id, 'kegg'))

      var image = '<img src="'+e.data.node.attributes.structure_url+'" alt="Drug structure" width="80px" height="80px" class="img-thumbnail rounded mx-auto d-block mb-2 struc">'
      if(e.data.node.attributes.drugbank_id != "NA"){
        image = '<img src="https://www.drugbank.ca/structures/'+e.data.node.attributes.drugbank_id+'/image.svg" alt="Drug structure" width="80px" height="80px" class="img-thumbnail rounded mx-auto d-block mb-2 struc">'
      }

      $('.structure').html(image)

      /// Clicking
      var nodeId = e.data.node.id,
          toKeep = s.graph.neighbors(nodeId);
      toKeep[nodeId] = e.data.node;

      // Mine
      function getEdge(s, t) {
        var w;
        sg.graph.edges().forEach(function(e) {
          if (e.source === s && e.target === t){
            w = e.weight
          }
          if(w === undefined){

            if (e.source === t && e.target === s){
              w = e.weight
            }

          }
        });
        return w
      }

      $table = $('.node-edges > tbody:last')
      $table.empty()

      s.graph.nodes().forEach(function(n) {
        if (toKeep[n.id]){
          n.color = n.originalColor;
          // Mine
          var wt = getEdge(n.id, nodeId), note = "-"

          if(wt >= 0.5){
            note = 'strong'
          }
          if(wt != undefined){
            $table.last().append('<tr><td><a href="#'+n.id+'" onclick="activateNode(\'' + n.id + '\')" onmouseover="hoverNode(\'' + n.id + '\')" onmouseout="deactivateNode(\'' + n.id + '\')">'+ n.label +'</a></td><td class="text-center">'+ wt.toFixed(3) +'</td><td>'+ note +'</td></tr>')
          }
        }else
          n.color = '#eee';
      });

      s.graph.edges().forEach(function(e) {
        if (toKeep[e.source] && toKeep[e.target])
          e.color = e.originalColor;
        else
          e.color = '#eee';
      });


      // Since the data has been modified, we need to
      // call the refresh method to make the colors
      // update effective.
      s.refresh();
      s.active = e
      window.location.hash = e.data.node.label;

       }, 200);
    });


    // When the stage is clicked, we just color each
    // node and edge with its original color.
    // s.bind('clickStage', function(e) {
    //   s.graph.nodes().forEach(function(n) {
    //     n.color = n.originalColor;
    //   });

    //   s.graph.edges().forEach(function(e) {
    //     e.color = e.originalColor;
    //   });

    //   // Same as in the previous event:
    //   s.refresh();
    // });

    $("#zoom").find("div.zoom-icons").each(function () {
      var a = $(this),
          b = a.attr("rel");
      a.click(function () {

          var c = s.camera;
          if (b == "center") {
              s.cameras[0].goTo({
                x: 0,
                y: 0,
                angle: 0,
                ratio: 1
              });
          } else {
              if(b == "out"){
                // Zoom out - single frame :
                c.goTo({
                  ratio: c.ratio * c.settings('zoomingRatio')
                });
              }
              else{
                // Zoom in - single frame :
                c.goTo({
                  ratio: c.ratio / c.settings('zoomingRatio')
                });
              }
          }
      })
  });

    document.getElementById('export-btn').onclick = function() {

      swal({
        title: "Export SVG image",
        text: "You want to export this network?",
        icon: "info",
        buttons: [true, "Yes"],
      })
      .then(function(exp){
        if (exp) {
          var output = s.toSVG({download: true, filename: 'dan-network.svg', size: 1000});
        } 
      });
      
    };

  /// Filters

  // Initialize the Filter API
      filter = new sigma.plugins.filter(s);

      updatePane(s.graph, filter);

      function applyMinDegreeFilter(e) {
        var v = e.target.value;
        _.$('min-degree-val').textContent = v;

        filter
          .undo('min-degree')
          .nodesBy(function(n) {
            return this.degree(n.id) >= v;
          }, 'min-degree')
          .apply();
      }

      function applyCategoryFilter(e) {
        var c = e.target[e.target.selectedIndex].value;
        filter
          .undo('node-category')
          .nodesBy(function(n) {
            return !c.length || n.attributes.atc_codes_letter === c;
          }, 'node-category')
          .apply();
      }

      _.$('min-degree').addEventListener("input", applyMinDegreeFilter);
      _.$('min-degree').addEventListener("change", applyMinDegreeFilter);
      _.$('node-category').addEventListener("change", applyCategoryFilter);
  }
);

$( window ).on( "load", function() {
  setTimeout(function(){ $(".se-pre-con").hide("slow"); }, 1000);
});


$('img').each(function(){
  $(this).attr('src', $(this).attr('delayedsrc'));
});