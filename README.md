## Drug Association Network (DAN)

The interactive visualizations shows drug signature profiles in the LINCs project. The network shows drug association in different modules. We analyses 2 different dataset containing Approved drugs, Non-approved drugs and 5 different cell type dataset.

## Demo

![DAN network](public/img/dan.png)

## License

The DAN network is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).

