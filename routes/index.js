var express = require('express');
var router = express.Router();
// Static pages
/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Home' });
});

/* GET contact page. */
router.get('/contact-us', function(req, res, next) {
  res.render('contact-us', { title: 'Contact Us' });
});

/* GET guide line page. */
router.get('/overview', function(req, res, next) {
  res.render('overview', { title: 'Overview' });
});

// Dynamic pages

/* GET approved drugs. */
router.get('/approved-drugs', function(req, res, next) {
  res.render('approved-drugs', {
  	title: 'FDA-Approved Drugs',
  	fileName: '/graph/approved-atc-new.gexf',
  	fieldName: 'atc_codes_letter',
  	viewType: 'ATC class of drugs.'
  });
});

/* GET approved modules. */
router.get('/approved-modules', function(req, res, next) {
  res.render('approved-drugs', {
  	title: 'FDA-Approved Drug Modules',
  	fileName: '/graph/approved-modules.gexf',
  	fieldName: 'atc_codes_letter',
  	viewType: 'cluster of drugs.'
  });
});

/* GET all drugs. */
router.get('/all-drugs', function(req, res, next) {
  res.render('all-drugs', {
  	title: 'All Drugs',
  	fileName: '/graph/all-atcs.gexf',
  	fieldName: 'atc_codes_letter',
  	viewType: 'ATC class of drugs.'
  });
});

/* GET all modules. */
router.get('/all-modules', function(req, res, next) {
  res.render('all-drugs', {
  	title: 'Drug Modules',
  	fileName: '/graph/all-modules.gexf',
  	fieldName: 'atc_codes_letter',
  	viewType: 'cluster of drugs.'
  });
});

/* GET cell type ATC. */
router.get('/cell-type-atc', function(req, res, next) {
  res.render('cell-type-atc', { title: 'Cell line' });
});

/* GET cell type network modules. */
router.get('/cell-type-modules', function(req, res, next) {
  res.render('cell-type-module', { title: 'Cell line Modules' });
});

// ATC class

/* GET MCF7 drugs. */
router.get('/cell-line/mcf7', function(req, res, next) {
  res.render('all-drugs', {
  	title: 'MCF7 Drugs',
  	fileName: '/graph/cell-line-mcf7.gexf',
  	fieldName: 'atc_codes_letter',
  	viewType: 'ATC class of drugs.'
  });
});

/* GET VCAP drugs. */
router.get('/cell-line/vcap', function(req, res, next) {
  res.render('all-drugs', {
  	title: 'VCAP Drugs',
  	fileName: '/graph/cell-line-vcap.gexf',
  	fieldName: 'atc_codes_letter',
  	viewType: 'ATC class of drugs.'
  });
});

/* GET PC3 drugs. */
router.get('/cell-line/pc3', function(req, res, next) {
  res.render('all-drugs', {
  	title: 'PC3 Drugs',
  	fileName: '/graph/cell-line-pc3.gexf',
  	fieldName: 'atc_codes_letter',
  	viewType: 'ATC class of drugs.'
  });
});

/* GET A549 drugs. */
router.get('/cell-line/a549', function(req, res, next) {
  res.render('all-drugs', {
  	title: 'A549 Drugs',
  	fileName: '/graph/cell-line-a549.gexf',
  	fieldName: 'atc_codes_letter',
  	viewType: 'ATC class of drugs.'
  });
});

/* GET A375 drugs. */
router.get('/cell-line/a375', function(req, res, next) {
  res.render('all-drugs', {
  	title: 'A375 Drugs',
  	fileName: '/graph/cell-line-a375.gexf',
  	fieldName: 'atc_codes_letter',
  	viewType: 'ATC class of drugs.'
  });
});

// Modules

/* GET MCF7 drugs. */
router.get('/cell-line-module/mcf7', function(req, res, next) {
  res.render('all-drugs', {
  	title: 'MCF7 Drugs',
  	fileName: '/graph/cell-line-module-mcf7.gexf',
  	fieldName: 'atc_codes_letter',
  	viewType: 'cluster of drugs.'
  });
});

/* GET VCAP drugs. */
router.get('/cell-line-module/vcap', function(req, res, next) {
  res.render('all-drugs', {
  	title: 'VCAP Drugs',
  	fileName: '/graph/cell-line-module-vcap.gexf',
  	fieldName: 'atc_codes_letter',
  	viewType: 'cluster of drugs.'
  });
});

/* GET PC3 drugs. */
router.get('/cell-line-module/pc3', function(req, res, next) {
  res.render('all-drugs', {
  	title: 'PC3 Drugs',
  	fileName: '/graph/cell-line-module-pc3.gexf',
  	fieldName: 'atc_codes_letter',
  	viewType: 'cluster of drugs.'
  });
});

/* GET A549 drugs. */
router.get('/cell-line-module/a549', function(req, res, next) {
  res.render('all-drugs', {
  	title: 'A549 Drugs',
  	fileName: '/graph/cell-line-module-a549.gexf',
  	fieldName: 'atc_codes_letter',
  	viewType: 'cluster of drugs.'
  });
});

/* GET A375 drugs. */
router.get('/cell-line-module/a375', function(req, res, next) {
  res.render('all-drugs', {
  	title: 'A375 Drugs',
  	fileName: '/graph/cell-line-module-a375.gexf',
  	fieldName: 'atc_codes_letter',
  	viewType: 'cluster of drugs.'
  });
});

module.exports = router;
